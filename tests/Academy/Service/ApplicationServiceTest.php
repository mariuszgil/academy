<?php

namespace Tests\Academy\Service;

use Academy\Entity\ApplicationStatus;
use Academy\Entity\ExamResult;
use Academy\Entity\IdentityNumberType;
use Academy\Entity\Type;
use Academy\Repository\Applications;
use Academy\Repository\Departments;
use Academy\Repository\Students;
use Academy\Service\ApplicationService;
use Academy\Service\DepartmentService;
use Academy\Service\NotificationService;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class ApplicationServiceTest extends TestCase
{
    private Students $students;
    private Departments $departments;
    private Applications $applications;

    private ApplicationService $applicationService;

    private DepartmentService $departmentService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->students = new \Academy\Repository\Students\InMemory();
        $this->departments = new \Academy\Repository\Departments\InMemory();
        $this->applications = new \Academy\Repository\Applications\InMemory();

        $this->departmentService = new DepartmentService($this->departments);
        $this->applicationService = new ApplicationService(new NotificationService(), $this->applications, $this->students, $this->departments);
    }

    public function testApplicationCanBeCreated()
    {
        $numberType = IdentityNumberType::PESEL;
        $number = '808080808080';

        $applicationId = $this->applicationService->createApplication(
            'Tester',
            'Testerowaty',
            $numberType,
            $number,
            'test@test.pl',
            [new ExamResult('przedmiot', 2.0), new ExamResult('przedmiot', 3.0)]
        );

        $student = $this->students->findBy($numberType, $number);
        $application = $this->applications->get($applicationId);

        $this->assertEquals($student->getId(), $application->getStudentId());
        $this->assertEquals(ApplicationStatus::DRAFT, $application->getStatus());
        $this->assertEquals(2.5, $application->getAverageScore());
    }

    public function testAnotherApplicationWillUpdateStudentInfo(): void
    {
        $numberType = IdentityNumberType::PESEL;
        $number = '808080808080';

        $applicationId1 = $this->applicationService->createApplication(
            'Tester',
            'Testerowaty',
            $numberType,
            $number,
            'test@test.pl',
            [new ExamResult('przedmiot', 2.0), new ExamResult('przedmiot', 3.0)]
        );

        $applicationId2 = $this->applicationService->createApplication(
            'Tester2',
            'Testerowaty2',
            $numberType,
            $number,
            'test@test2.pl',
            [new ExamResult('przedmiot', 2.0), new ExamResult('przedmiot', 3.0)]
        );

        $student = $this->students->findBy($numberType, $number);
        $application1 = $this->applications->get($applicationId1);
        $application2 = $this->applications->get($applicationId2);

        $this->assertEquals($student->getId(), $application1->getStudentId());
        $this->assertEquals($student->getId(), $application2->getStudentId());
        $this->assertEquals('Tester2', $student->getFirstName());
        $this->assertEquals('Testerowaty2', $student->getLastName());
    }

    public function testApplicationWithIncorrectExamResultsCannotBeCreated(): void
    {
        $this->expectException(\Exception::class);

        $numberType = IdentityNumberType::PESEL;
        $number = '808080808080';

        $applicationId = $this->applicationService->createApplication(
            'Tester',
            'Testerowaty',
            $numberType,
            $number,
            'test@test.pl',
            [new ExamResult('przedmiot', -2.0), new ExamResult('przedmiot', 3.0)]
        );
    }

    public function testNotExistingApplicationCannotBeModified(): void
    {
        $this->expectException(\Exception::class);

        $this->applicationService->updateApplication(Uuid::uuid4(), [new ExamResult('przedmiot', 5.0)]);
    }

    public function testIncorrectExamResultsCannotBeModified(): void
    {
        $this->expectException(\Exception::class);

        $applicationId = $this->applicationService->createApplication(
            'Tester',
            'Testerowaty',
            IdentityNumberType::PESEL,
            '808080808080',
            'test@test.pl',
            [new ExamResult('przedmiot', 2.0), new ExamResult('przedmiot', 3.0)]
        );

        $this->applicationService->updateApplication($applicationId, [new ExamResult('przedmiot', -5.0)]);
    }

    public function testExamCanBeUpdatedBeforeSubmission(): void
    {
        $applicationId = $this->applicationService->createApplication(
            'Tester',
            'Testerowaty',
            IdentityNumberType::PESEL,
            '808080808080',
            'test@test.pl',
            [new ExamResult('przedmiot', 2.0), new ExamResult('przedmiot', 3.0)]
        );

        $this->applicationService->updateApplication($applicationId, [new ExamResult('przedmiot', 5.0), new ExamResult('przedmiot', 3.0)]);

        $application = $this->applications->get($applicationId);

        $this->assertEquals(4.0, $application->getAverageScore());
    }

    public function testExamResultsCannotBeModifiedAfterSubmission()
    {
        $this->expectException(\Exception::class);

        $departmentId = $this->departmentService->createDepartment('Test department', 'Test description');
        $this->departmentService->prepareForSubmissions($departmentId, 2022, Type::FLOW_SUPPLEMENTARY, 1);
        $this->departmentService->startAcceptApplications($departmentId, 2022, Type::FLOW_SUPPLEMENTARY);
        $applicationId = $this->applicationService->createApplication(
            'Tester',
            'Testerowaty',
            IdentityNumberType::PESEL,
            '808080808080',
            'test@test.pl',
            [new ExamResult('przedmiot', 2.0), new ExamResult('przedmiot', 3.0)]
        );

        $this->applicationService->submitApplication($applicationId, $departmentId, 2022, Type::FLOW_SUPPLEMENTARY);

        $this->applicationService->updateApplication($applicationId, [new ExamResult('przedmiot', 5.0), new ExamResult('przedmiot', 3.0)]);
    }

    public function testApplicationCanBeSubmitted()
    {
        $departmentId = $this->departmentService->createDepartment('Test department', 'Test description');
        $this->departmentService->prepareForSubmissions($departmentId, 2022, Type::FLOW_SUPPLEMENTARY, 1);
        $this->departmentService->startAcceptApplications($departmentId, 2022, Type::FLOW_SUPPLEMENTARY);
        $applicationId = $this->applicationService->createApplication(
            'Tester',
            'Testerowaty',
            IdentityNumberType::PESEL,
            '808080808080',
            'test@test.pl',
            [new ExamResult('przedmiot', 2.0), new ExamResult('przedmiot', 3.0)]
        );

        $this->applicationService->submitApplication($applicationId, $departmentId, 2022, Type::FLOW_SUPPLEMENTARY);

        $this->assertTrue($this->applicationService->acceptApplication($applicationId));
    }

    public function testApplicationCannotBeSubmittedToNonExistingDepartment()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Department with given id does not exist');

        $applicationId = $this->applicationService->createApplication(
            'Tester',
            'Testerowaty',
            IdentityNumberType::PESEL,
            '808080808080',
            'test@test.pl',
            [new ExamResult('przedmiot', 2.0), new ExamResult('przedmiot', 3.0)]
        );

        $this->applicationService->submitApplication($applicationId, Uuid::uuid4(), 2022, Type::FLOW_SUPPLEMENTARY);
    }

    public function testNonExistingApplicationCannotBeSubmitted()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Application with given id does not exist');

        $departmentId = $this->departmentService->createDepartment('Test department', 'Test description');
        $this->departmentService->prepareForSubmissions($departmentId, 2022, Type::FLOW_SUPPLEMENTARY, 1);
        $this->departmentService->startAcceptApplications($departmentId, 2022, Type::FLOW_SUPPLEMENTARY);

        $this->applicationService->submitApplication(Uuid::uuid4(), $departmentId, 2022, Type::FLOW_SUPPLEMENTARY);
    }

    public function testApplicationCanBeSubmittedOnlyOnce()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Application already submitted');

        $departmentId = $this->departmentService->createDepartment('Test department', 'Test description');
        $this->departmentService->prepareForSubmissions($departmentId, 2022, Type::FLOW_SUPPLEMENTARY, 1);
        $this->departmentService->startAcceptApplications($departmentId, 2022, Type::FLOW_SUPPLEMENTARY);
        $applicationId = $this->applicationService->createApplication(
            'Tester',
            'Testerowaty',
            IdentityNumberType::PESEL,
            '808080808080',
            'test@test.pl',
            [new ExamResult('przedmiot', 2.0), new ExamResult('przedmiot', 3.0)]
        );
        $this->applicationService->submitApplication($applicationId, $departmentId, 2022, Type::FLOW_SUPPLEMENTARY);

        $this->applicationService->submitApplication($applicationId, $departmentId, 2022, Type::FLOW_SUPPLEMENTARY);
    }

    public function testApplicationCannotBeSubmittedIfSubmissionsWereNotOpen()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Department does not accept applications now');

        $departmentId = $this->departmentService->createDepartment('Test department', 'Test description');
        $this->departmentService->prepareForSubmissions($departmentId, 2022, Type::FLOW_SUPPLEMENTARY, 1);
        $applicationId = $this->applicationService->createApplication(
            'Tester',
            'Testerowaty',
            IdentityNumberType::PESEL,
            '808080808080',
            'test@test.pl',
            [new ExamResult('przedmiot', 2.0), new ExamResult('przedmiot', 3.0)]
        );

        $this->applicationService->submitApplication($applicationId, $departmentId, 2022, Type::FLOW_SUPPLEMENTARY);
    }

    public function testApplicationCannotBeSubmittedIfSubmissionsWereClosed()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Department does not accept applications now');

        $departmentId = $this->departmentService->createDepartment('Test department', 'Test description');
        $this->departmentService->prepareForSubmissions($departmentId, 2022, Type::FLOW_SUPPLEMENTARY, 1);
        $this->departmentService->startAcceptApplications($departmentId, 2022, Type::FLOW_SUPPLEMENTARY);
        $this->departmentService->stopAcceptApplications($departmentId, 2022, Type::FLOW_SUPPLEMENTARY);
        $applicationId = $this->applicationService->createApplication(
            'Tester',
            'Testerowaty',
            IdentityNumberType::PESEL,
            '808080808080',
            'test@test.pl',
            [new ExamResult('przedmiot', 2.0), new ExamResult('przedmiot', 3.0)]
        );

        $this->applicationService->submitApplication($applicationId, $departmentId, 2022, Type::FLOW_SUPPLEMENTARY);
    }

    /**
     * @return void
     * @throws \Exception
     * @group multi
     */
    public function testApplicationCannotBeSubmittedWhenCapacityWasReached()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Application cannot be submitted, max capacity reached');

        $departmentId = $this->departmentService->createDepartment('Test department', 'Test description');
        $this->departmentService->prepareForSubmissions($departmentId, 2022, Type::FLOW_SUPPLEMENTARY, 1);
        $this->departmentService->startAcceptApplications($departmentId, 2022, Type::FLOW_SUPPLEMENTARY);
        $applicationId1 = $this->applicationService->createApplication(
            'Tester',
            'Testerowaty',
            IdentityNumberType::PESEL,
            '808080808080',
            'test@test.pl',
            [new ExamResult('przedmiot', 2.0), new ExamResult('przedmiot', 3.0)]
        );
        $applicationId2 = $this->applicationService->createApplication(
            'Tester2',
            'Testerowaty2',
            IdentityNumberType::PESEL,
            '909090909090',
            'test@test2.pl',
            [new ExamResult('przedmiot', 2.0), new ExamResult('przedmiot', 3.0)]
        );
        $this->applicationService->submitApplication($applicationId1, $departmentId, 2022, Type::FLOW_SUPPLEMENTARY);
        $this->applicationService->acceptApplication($applicationId1);

        $this->applicationService->submitApplication($applicationId2, $departmentId, 2022, Type::FLOW_SUPPLEMENTARY);
    }

    public function testSubmittedApplicationCanBeAccepted()
    {
        $departmentId = $this->departmentService->createDepartment('Test department', 'Test description');
        $this->departmentService->prepareForSubmissions($departmentId, 2022, Type::FLOW_SUPPLEMENTARY, 1);
        $this->departmentService->startAcceptApplications($departmentId, 2022, Type::FLOW_SUPPLEMENTARY);
        $applicationId = $this->applicationService->createApplication(
            'Tester',
            'Testerowaty',
            IdentityNumberType::PESEL,
            '808080808080',
            'test@test.pl',
            [new ExamResult('przedmiot', 2.0), new ExamResult('przedmiot', 3.0)]
        );
        $this->applicationService->submitApplication($applicationId, $departmentId, 2022, Type::FLOW_SUPPLEMENTARY);

        $result = $this->applicationService->acceptApplication($applicationId);

        $this->assertTrue($result);
        $this->assertEquals(ApplicationStatus::ACCEPTED, $this->applications->get($applicationId)->getStatus());
    }

    public function testSubmittedApplicationCanBeRejected()
    {
        $departmentId = $this->departmentService->createDepartment('Test department', 'Test description');
        $this->departmentService->prepareForSubmissions($departmentId, 2022, Type::FLOW_SUPPLEMENTARY, 1);
        $this->departmentService->startAcceptApplications($departmentId, 2022, Type::FLOW_SUPPLEMENTARY);
        $applicationId = $this->applicationService->createApplication(
            'Tester',
            'Testerowaty',
            IdentityNumberType::PESEL,
            '808080808080',
            'test@test.pl',
            [new ExamResult('przedmiot', 2.0), new ExamResult('przedmiot', 3.0)]
        );
        $this->applicationService->submitApplication($applicationId, $departmentId, 2022, Type::FLOW_SUPPLEMENTARY);

        $result = $this->applicationService->rejectApplication($applicationId);

        $this->assertTrue($result);
        $this->assertEquals(ApplicationStatus::REJECTED, $this->applications->get($applicationId)->getStatus());
    }

    public function testRejectingAcceptedApplicationRestoreUsedCapacity()
    {
        $departmentId = $this->departmentService->createDepartment('Test department', 'Test description');
        $this->departmentService->prepareForSubmissions($departmentId, 2022, Type::FLOW_SUPPLEMENTARY, 1);
        $this->departmentService->startAcceptApplications($departmentId, 2022, Type::FLOW_SUPPLEMENTARY);
        $applicationId1 = $this->applicationService->createApplication(
            'Tester',
            'Testerowaty',
            IdentityNumberType::PESEL,
            '808080808080',
            'test@test.pl',
            [new ExamResult('przedmiot', 2.0), new ExamResult('przedmiot', 3.0)]
        );
        $applicationId2 = $this->applicationService->createApplication(
            'Tester2',
            'Testerowaty2',
            IdentityNumberType::PESEL,
            '909090909090',
            'test@test2.pl',
            [new ExamResult('przedmiot', 2.0), new ExamResult('przedmiot', 3.0)]
        );
        $this->applicationService->submitApplication($applicationId1, $departmentId, 2022, Type::FLOW_SUPPLEMENTARY);
        $this->applicationService->acceptApplication($applicationId1);
        $this->applicationService->rejectApplication($applicationId1);

        $result = $this->applicationService->submitApplication($applicationId2, $departmentId, 2022, Type::FLOW_SUPPLEMENTARY);

        $this->assertTrue($result);
        $this->assertEquals(ApplicationStatus::SUBMITTED, $this->applications->get($applicationId2)->getStatus());
    }

    public function testNotSubmittedApplicationCannotBeRejected()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Application cannot be rejected');

        $departmentId = $this->departmentService->createDepartment('Test department', 'Test description');
        $this->departmentService->prepareForSubmissions($departmentId, 2022, Type::FLOW_SUPPLEMENTARY, 1);
        $this->departmentService->startAcceptApplications($departmentId, 2022, Type::FLOW_SUPPLEMENTARY);
        $applicationId = $this->applicationService->createApplication(
            'Tester',
            'Testerowaty',
            IdentityNumberType::PESEL,
            '808080808080',
            'test@test.pl',
            [new ExamResult('przedmiot', 2.0), new ExamResult('przedmiot', 3.0)]
        );

        $this->applicationService->rejectApplication($applicationId);
    }

    public function testApplicationCannotBeRejectedTwice()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Application cannot be rejected');

        $departmentId = $this->departmentService->createDepartment('Test department', 'Test description');
        $this->departmentService->prepareForSubmissions($departmentId, 2022, Type::FLOW_SUPPLEMENTARY, 1);
        $this->departmentService->startAcceptApplications($departmentId, 2022, Type::FLOW_SUPPLEMENTARY);
        $applicationId = $this->applicationService->createApplication(
            'Tester',
            'Testerowaty',
            IdentityNumberType::PESEL,
            '808080808080',
            'test@test.pl',
            [new ExamResult('przedmiot', 2.0), new ExamResult('przedmiot', 3.0)]
        );
        $this->applicationService->submitApplication($applicationId, $departmentId, 2022, Type::FLOW_SUPPLEMENTARY);
        $this->applicationService->rejectApplication($applicationId);

        $this->applicationService->rejectApplication($applicationId);
    }
}