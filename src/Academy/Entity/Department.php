<?php

namespace Academy\Entity;

use Ramsey\Uuid\UuidInterface;

class Department
{
    private UuidInterface $id;

    private string $name;

    private string $description;

    /**
     * @var SubmissionsFlowData[]
     */
    private array $submissionsFlowData = [];

    public function __construct()
    {
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @param UuidInterface $id
     */
    public function setId(UuidInterface $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function getSubmissionsFlowData(): array
    {
        return $this->submissionsFlowData;
    }

    /**
     * @param array $submissionsFlowData
     */
    public function setSubmissionsFlowData(array $submissionsFlowData): void
    {
        $this->submissionsFlowData = $submissionsFlowData;
    }

    public function canAcceptApplicationsFor(int $year, Type $type): bool
    {
        if (!array_key_exists($year . '-' . $type->value, $this->submissionsFlowData)) {
            throw new \Exception('Submission not configured');
        }

        return $this->submissionsFlowData[$year . '-' . $type->value]->isOpenForSubmissions();
    }

    public function getAcceptedApplicationsCounter(int $year, Type $type): int
    {
        if (!array_key_exists($year . '-' . $type->value, $this->submissionsFlowData)) {
            throw new \Exception('Submission not configured');
        }

        return $this->submissionsFlowData[$year . '-' . $type->value]->getAcceptedApplicationsCounter();
    }

    public function setAcceptedApplicationsCounter(int $year, Type $type, int $counter): void
    {
        if (!array_key_exists($year . '-' . $type->value, $this->submissionsFlowData)) {
            throw new \Exception('Submission not configured');
        }

        $this->submissionsFlowData[$year . '-' . $type->value]->setAcceptedApplicationsCounter($counter);
    }

    public function getCapacity(int $year, Type $type): int
    {
        if (!array_key_exists($year . '-' . $type->value, $this->submissionsFlowData)) {
            throw new \Exception('Submission not configured');
        }

        return $this->submissionsFlowData[$year . '-' . $type->value]->getCapacity();
    }
}