<?php

namespace Academy\Entity;

enum StudentStatus
{
    case CANDIDATE;
    case LEARNING;
    case FINISHED;
    case REMVOED;
}