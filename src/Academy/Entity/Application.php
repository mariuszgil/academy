<?php

namespace Academy\Entity;

use Ramsey\Uuid\UuidInterface;

class Application
{
    private UuidInterface $id;

    private ApplicationStatus $status;

    private \DateTimeImmutable $createdAt;

    private array $examsResults;

    private \DateTimeImmutable $submittedAt;

    private UuidInterface $departmentId;

    private int $year;

    private Type $type;

    private \DateTimeImmutable $processedAt;

    private UuidInterface $studentId;

    /**
     * @var ExamResult[]
     */
    private array $examResults;

    private float $averageScore;

    public function __construct()
    {
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @param UuidInterface $id
     */
    public function setId(UuidInterface $id): void
    {
        $this->id = $id;
    }

    /**
     * @return ApplicationStatus
     */
    public function getStatus(): ApplicationStatus
    {
        return $this->status;
    }

    /**
     * @param ApplicationStatus $status
     */
    public function setStatus(ApplicationStatus $status): void
    {
        $this->status = $status;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeImmutable $createdAt
     */
    public function setCreatedAt(\DateTimeImmutable $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return array
     */
    public function getExamsResults(): array
    {
        return $this->examsResults;
    }

    /**
     * @param array $examsResults
     */
    public function setExamsResults(array $examsResults): void
    {
        $this->examsResults = $examsResults;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getSubmittedAt(): \DateTimeImmutable
    {
        return $this->submittedAt;
    }

    /**
     * @param \DateTimeImmutable $submittedAt
     */
    public function setSubmittedAt(\DateTimeImmutable $submittedAt): void
    {
        $this->submittedAt = $submittedAt;
    }

    /**
     * @return UuidInterface
     */
    public function getDepartmentId(): UuidInterface
    {
        return $this->departmentId;
    }

    /**
     * @param UuidInterface $departmentId
     */
    public function setDepartmentId(UuidInterface $departmentId): void
    {
        $this->departmentId = $departmentId;
    }

    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->year;
    }

    /**
     * @param int $year
     */
    public function setYear(int $year): void
    {
        $this->year = $year;
    }

    /**
     * @return Type
     */
    public function getType(): Type
    {
        return $this->type;
    }

    /**
     * @param Type $type
     */
    public function setType(Type $type): void
    {
        $this->type = $type;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getProcessedAt(): \DateTimeImmutable
    {
        return $this->processedAt;
    }

    /**
     * @param \DateTimeImmutable $processedAt
     */
    public function setProcessedAt(\DateTimeImmutable $processedAt): void
    {
        $this->processedAt = $processedAt;
    }

    /**
     * @return UuidInterface
     */
    public function getStudentId(): UuidInterface
    {
        return $this->studentId;
    }

    /**
     * @param UuidInterface $studentId
     */
    public function setStudentId(UuidInterface $studentId): void
    {
        $this->studentId = $studentId;
    }

    /**
     * @return array
     */
    public function getExamResults(): array
    {
        return $this->examResults;
    }

    /**
     * @param array $examResults
     */
    public function setExamResults(array $examResults): void
    {
        $this->examResults = $examResults;
    }

    /**
     * @return float
     */
    public function getAverageScore(): float
    {
        return $this->averageScore;
    }

    /**
     * @param float $averageScore
     */
    public function setAverageScore(float $averageScore): void
    {
        $this->averageScore = $averageScore;
    }
}