<?php

namespace Academy\Entity;

enum ApplicationStatus
{
    case DRAFT;
    case SUBMITTED;
    case ACCEPTED;
    case REJECTED;
}