<?php

namespace Academy\Entity;

class ExamResult
{
    private string $subject;

    private float $result;

    /**
     * @param string $subject
     * @param float $result
     */
    public function __construct(string $subject, float $result)
    {
        $this->subject = $subject;
        $this->result = $result;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return float
     */
    public function getResult(): float
    {
        return $this->result;
    }

    /**
     * @param float $result
     */
    public function setResult(float $result): void
    {
        $this->result = $result;
    }
}