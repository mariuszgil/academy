<?php

namespace Academy\Entity;

enum SubmissionsFlowStatus
{
    case DRAFT;
    case OPEN;
    case CLOSED;
    case CANCELLED;
}