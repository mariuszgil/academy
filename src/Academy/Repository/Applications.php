<?php

namespace Academy\Repository;

use Academy\Entity\Application;
use Academy\Entity\Type;
use Ramsey\Uuid\UuidInterface;

interface Applications
{
    public function get(UuidInterface $id): ?Application;

    public function save(Application $application): void;
}