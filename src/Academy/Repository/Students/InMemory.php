<?php

namespace Academy\Repository\Students;

use Academy\Entity\IdentityNumberType;
use Academy\Entity\Student;
use Academy\Repository\Students;
use Ramsey\Uuid\UuidInterface;

class InMemory implements Students
{
    /**
     * @var Student[]
     */
    private array $students = [];

    public function get(UuidInterface $id): ?Student
    {
        return $this->students[$id->toString()] ?? null;
    }

    public function save(Student $student): void
    {
        $this->students[$student->getId()->toString()] = $student;
    }

    public function findBy(IdentityNumberType $numberType, string $identityNumber) : ?Student
    {
        foreach ($this->students as $student) {
            if ($student->getIdentityNumber() === $identityNumber && $student->getNumberType() === $numberType) {
                return $student;
            }
        }

        return null;
    }
}