<?php

namespace Academy\Repository\Applications;

use Academy\Entity\Application;
use Academy\Entity\Type;
use Academy\Repository\Applications;
use Ramsey\Uuid\UuidInterface;

class InMemory implements Applications
{
    /**
     * @var Application[]
     */
    private array $applications = [];

    public function get(UuidInterface $id): ?Application
    {
        return $this->applications[$id->toString()] ?? null;
    }

    public function save(Application $application): void
    {
        $this->applications[$application->getId()->toString()] = $application;
    }
}