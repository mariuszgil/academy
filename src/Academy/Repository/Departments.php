<?php

namespace Academy\Repository;

use Academy\Entity\Department;
use Ramsey\Uuid\UuidInterface;

interface Departments
{
    public function get(UuidInterface $id): ?Department;

    public function save(Department $department): void;
}