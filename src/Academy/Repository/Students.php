<?php

namespace Academy\Repository;

use Academy\Entity\IdentityNumberType;
use Academy\Entity\Student;
use Ramsey\Uuid\UuidInterface;

interface Students
{
    public function get(UuidInterface $id): ?Student;

    public function save(Student $student): void;

    public function findBy(IdentityNumberType $numberType, string $identityNumber);
}